function playRound() {
    var player1 = getRandomShape();
    var player2 = "Rock";
    requestToPlayRound(player1, player2);

}

function getRandomShape() {
    var random = Math.floor(Math.random() * 3);

    var shape = "Rock";
    if(random == 1)
        shape = "Paper";
    else if(random == 2)
        shape = "Scissors";
    return shape;
}


function requestToPlayRound(player1, player2) {
    var url = 'http://localhost:9090/playround?player1=' + player1 + '&player2=' + player2;
    $.ajax({
        url: url,
        dataType: 'json',
        success: function (response) {
            updateResults(player1, player2, response.message);
        },
        error: function (xhr, status, error) {
            console.log(status + '; ' + error);
        }
    });
}

function updateResults(player1, player2, winner) {
    var currentRoundsElement = document.getElementById('roundsPlayed');
    var round = parseInt(currentRoundsElement.innerHTML) + 1;
    updateRoundsPlayed(round);
    addResultRow(round, player1, player2, winner)
}

function updateRoundsPlayed(value) {
    var currentRoundsElement = document.getElementById('roundsPlayed');
    currentRoundsElement.innerHTML = value;
}

function resetRounds() {
    updateRoundsPlayed(0);
    cleanTableResults();
}

function cleanTableResults() {
    var tableBody = document.getElementById("tableBody");
    tableBody.innerHTML = "";
}

function addResultRow(round, player1, player2, winner) {

    var tableBody = document.getElementById("tableBody");

    // insert new row.
    var row = tableBody.insertRow(0);
    var roundCell = row.insertCell(0);
    var player1Cell = row.insertCell(1);
    var player2Cell = row.insertCell(2);
    var winnerCell = row.insertCell(3);

    roundCell.innerHTML = round;
    player1Cell.innerHTML = player1;
    player2Cell.innerHTML = player2;
    winnerCell.innerHTML = winner;
 }