function showStatistics() {
    requestStatistics();
    document.getElementById("game").style.display = "none";
    document.getElementById("stats").style.display = "block";
 }

 function backToGame() {
     document.getElementById("game").style.display = "block";
     document.getElementById("stats").style.display = "none";
  }


function requestStatistics() {

    var url = "http://localhost:9090/statistics";
    $.ajax({
            url: url,
            dataType: 'json',
            success: function (response) {
                updateStatisticsResults(response.totalRoundPlayed, response.totalDraws, response.totalPlayer1Wins, response.totalPlayer2Wins);
            },
            error: function (xhr, status, error) {
                console.log(status + '; ' + error);
            }
        });
}

function updateStatisticsResults(totalRoundPlayed, totalDraws, totalPlayer1Wins, totalPlayer2Wins) {
    var roundsPlayedElement = document.getElementById('totalRoundsPlayed');
    var totalPlayer1WinsElement = document.getElementById('totalPlayer1Wins');
    var totalPlayer2WinsElement = document.getElementById('totalPlayer2Wins');
    var totalDrawsElement = document.getElementById('totalDraws');

    roundsPlayedElement.innerHTML = totalRoundPlayed;
    totalPlayer1WinsElement.innerHTML = totalPlayer1Wins;
    totalPlayer2WinsElement.innerHTML = totalPlayer2Wins;
    totalDrawsElement.innerHTML = totalDraws;
}